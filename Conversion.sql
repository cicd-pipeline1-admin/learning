USE [demos]
GO

/****** Object:  Table [dbo].[Expenses]    Script Date: 4/09/2021 7:31:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Conversion](
	[date] [datetime] NULL,
	[USD] [money] NULL,
	[rate] [decimal](6, 5) NULL,
	[CAD] [money] NULL,
	[CSK] [money] NULL,
    [RUB] [money] NULL,
	[INR] [money] NULL,
	[YEN] [money] NULL
) ON [PRIMARY]
GO


